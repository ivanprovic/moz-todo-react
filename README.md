MDN web docs, React tutorial
Follow the [link](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/React_getting_started)! 

App deployed **_[here](https://todo-react-green-three.vercel.app/)_**