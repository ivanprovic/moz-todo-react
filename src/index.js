import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";

const DATA = [
    {id: "todo-0", name: "Work", completed: true},
    {id: "todo-1", name: "Sleep", completed: false},
    {id: "todo-2", name: "Run", completed: false},
]

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<App tasks={DATA} />);
